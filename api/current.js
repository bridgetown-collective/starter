const router = require("../lib/utils/router")();

router.get("*", async (req, res) => {
  const member = req.member.toJSON();
  const user = req.user.toJSON();
  const workspace = req.workspace.toJSON();

  res.send({ member, user, workspace });
});

module.exports = router;
