const router = require("../lib/utils/router")({ public: true });

router.get("*", (req, res) => res.status(200).send("Success"));

module.exports = router;
