const responseMiddleware = require("../../lib/utils/response-middleware");
const router = require("../../lib/utils/router")();

router.post("*", async (req, res, next) => {
  try {
    await req.workspace.set({ name: req.body.name }).save();

    req.data = req.workspace.toJSON();
    req.message = "Success";
  } catch (e) {
    req.error = e;
  }
  next();
});

router.use(responseMiddleware);

module.exports = router;
