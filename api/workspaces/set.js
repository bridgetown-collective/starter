const responseMiddleware = require("../../lib/utils/response-middleware");
const router = require("../../lib/utils/router")();

router.get("*", async (req, res, next) => {
  res.cookie("tenant", req.query.tenant);
  req.status = 200;

  next();
});

router.use(responseMiddleware);

module.exports = router;
