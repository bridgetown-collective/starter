const db = require("../../lib/db");
const responseMiddleware = require("../../lib/utils/response-middleware");
const router = require("../../lib/utils/router")();

router.post("*", async (req, res, next) => {
  try {
    const workspace = await db.Workspace().create({ name: req.body.name });
    await db
      .Member({ tenant: workspace._id })
      .create({ email: req.user.get("email"), role: "ADMIN" });

    res.cookie("tenant", `${workspace._id}`);
    req.status = 200;
  } catch (e) {
    req.error = e;
  }
  next();
});

router.use(responseMiddleware);

module.exports = router;
