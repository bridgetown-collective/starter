const db = require("../../lib/db");
const responseMiddleware = require("../../lib/utils/response-middleware");
const router = require("../../lib/utils/router")();

router.get("*", async (req, res, next) => {
  const workspaces = (await db
    .Member({ skipTenant: true })
    .find({ email: req.user.get("email") })
    .select(["workspace"])
    .populate({ match: { deleted: false }, path: "workspace" }))
    .filter(m => m && m.get("workspace"))
    .map(m => m.get("workspace").toJSON());

  req.data = workspaces;

  next();
});

router.use(responseMiddleware);

module.exports = router;
