require("dotenv").config();
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

const responseMiddleware = require("../../lib/utils/response-middleware");
const router = require("../../lib/utils/router")();

router.all("*", async (req, res, next) => {
  try {
    await stripe.subscriptions.del(req.workspace.get("billing.subscription"));
    req.message = "Success";
  } catch (e) {
    req.error = e;
  }

  if (req.query.deleteWorkspace) {
    await req.workspace.set({ deleted: true }).save();
    req.status = 200;
  }

  next();
});

router.use(responseMiddleware);

module.exports = router;
