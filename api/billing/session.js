require("dotenv").config();
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

const responseMiddleware = require("../../lib/utils/response-middleware");
const router = require("../../lib/utils/router")();

router.get("*", async (req, res, next) => {
  try {
    if (!req.workspace.get("billing.customer")) {
      const customer = await stripe.customers.create({
        email: req.user.get("email"),
        name: req.workspace.get("name"),
      });

      await req.workspace.set({ "billing.customer": customer.id }).save();
    }

    const payload = {
      payment_method_types: ["card"],
      success_url: `${req.query.redirectPage}`,
      cancel_url: `${req.query.redirectPage}`,
    };

    if (req.query.plan === "update") {
      payload.mode = "setup";
      payload.setup_intent_data = {
        metadata: {
          customer_id: req.workspace.get("billing.customer"),
          subscription_id: req.workspace.get("billing.subscription"),
        },
      };
    } else {
      payload.customer = req.workspace.get("billing.customer");
      payload.subscription_data = {
        items: [
          {
            plan: req.query.plan,
          },
        ],
      };
    }

    const session = await stripe.checkout.sessions.create(payload);

    await req.workspace.set({ "billing.session": session.id }).save();

    req.data = { sessionId: session.id };
  } catch (e) {
    req.error = e;
  }

  next();
});

router.use(responseMiddleware);

module.exports = router;
