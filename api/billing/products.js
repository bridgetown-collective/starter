require("dotenv").config();
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

const router = require("../../lib/utils/router")({ public: true });

router.get("*", async (req, res) => {
  const { data: plans } = await stripe.plans.list({ active: true });
  const { data: products } = await stripe.products.list({ active: true });

  const payload = products
    .filter(p => p.name && p.name.startsWith("Starter"))
    .map(p => ({
      ...p,
      plans: plans.filter(p2 => p2.product === p.id),
    }));

  res.send(payload);
});

module.exports = router;
