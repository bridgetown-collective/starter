const theme = {
  colors: {
    brand: "#f05d5e",
  },
  font: {
    family: "Roboto",
    size: "14px",
    height: "20px",
  },
};

module.exports = theme;
