const { Schema } = require("mongoose");

const Workspace = new Schema({
  billing: Map,
  deleted: {
    default: false,
    type: Boolean,
  },
  meta: Map,
  plan: String,
  name: {
    required: true,
    type: String,
  },
});

module.exports = Workspace;
