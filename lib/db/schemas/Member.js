const { Schema } = require("mongoose");

const Member = new Schema({
  email: {
    index: true,
    required: true,
    type: String,
  },
  role: {
    default: "MEMBER",
    enum: ["ADMIN", "MEMBER"],
    index: true,
    required: true,
    type: String,
  },
});

module.exports = Member;
