module.exports = req => {
  if (!req) {
    return { cookies: {}, origin: "" };
  }

  const cookies = (req.headers.cookie || "")
    .split(";")
    .map(sub => sub.trim().split("="))
    .reduce((a, b) => ({ ...a, [b[0]]: global.decodeURI(b[1]) }), {});

  let origin = req.headers.host || "";
  if (!origin.startsWith("http")) {
    if (origin.startsWith("localhost")) {
      origin = `http://${origin}`;
    } else {
      origin = `https://${origin}`;
    }
  }

  return { cookies, origin };
};
