module.exports = (req, res) => {
  if (req.query.redirectTo) {
    const k = req.query.redirectTo.includes("?") ? "&" : "?";

    if (req.status === 200) {
      res.redirect(req.query.redirectTo);
    } else if (req.error || (!req.message && !req.data)) {
      res.redirect(
        `${req.query.redirectTo}${k}error=${(req.error && req.error.message) ||
          "Bad request"}`,
      );
    } else if (k === "?") {
      res.redirect(`${req.query.redirectTo}${k}message=${req.message}`);
    } else {
      res.redirect(req.query.redirectTo);
    }
    return;
  }

  if (req.status || (!req.data && !req.message)) {
    res
      .status(req.status || 400)
      .send({ error: (req.error && req.error.message) || "Bad Request" });
  } else {
    res.send(req.data || { message: req.message });
  }
};
