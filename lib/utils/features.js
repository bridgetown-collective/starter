const plansByName = {
  "Starter Subscription - Developer": "dev",
  "Starter Subscription - Pro": "pro",
};

const features = {
  LIMITED_WORKSPACE_MEMBERS: {
    label: "Up to 5 members in a workspace",
  },
  UNLIMITED_WORKSPACE_MEMBERS: {
    label: "Unlimited members in a workspace",
  },
};

const featuresByCode = {
  dev: ["LIMITED_WORKSPACE_MEMBERS"],
  pro: ["UNLIMITED_WORKSPACE_MEMBERS"],
};

const checkFeature = (plan, feature) => {
  if (!plan || !feature) {
    return false;
  }

  const code = plansByName[plan] || plan;

  if (featuresByCode[code] && featuresByCode[code].includes(feature)) {
    return true;
  }

  return false;
};

module.exports = {
  checkFeature,
  features,
  featuresByCode,
  plansByName,
};
