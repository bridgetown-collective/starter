const jwt = require("express-jwt");

const config = require("../config")();
const parseReq = require("./parse-req");

module.exports = jwt({
  issuer: config.AUTH0_DOMAIN,
  getToken: req => {
    if (
      req.headers.authorization &&
      req.headers.authorization.split(" ")[0] === "Bearer"
    ) {
      return req.headers.authorization.split(" ")[1];
    }

    try {
      const { cookies } = parseReq(req);

      if (cookies.authToken && cookies.authToken.split(" ")[0] === "Bearer") {
        return cookies.authToken.split(" ")[1];
      }
    } catch (e) {
      // ignore this
    }

    if (req.query && req.query.token) {
      return req.query.token;
    }

    return null;
  },
  secret: process.env.AUTH0_CLIENT_SECRET,
});
