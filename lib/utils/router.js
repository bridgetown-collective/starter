require("dotenv").config();
const bodyParser = require("body-parser");
const express = require("express");

const checkJwtMiddleware = require("./check-jwt-middleware");
const { bindCurrentNamespace, setCurrentTenantId } = require("./namespace");
const db = require("../db");
const parseReq = require("./parse-req");

module.exports = (opts = {}) => {
  const app = express();

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  if (!opts.public) {
    app.use(checkJwtMiddleware);
    app.use(async (req, res, next) => {
      if (req.user && req.user.sub && req.user.email) {
        const { email, name, picture } = req.user;

        req.user = await db.User().findOne({ email });

        if (!req.user) {
          req.user = await db.User().create({ avatar: picture, email, name });
        }
      }

      if (!req.user || !req.user.get("active")) {
        res.clearCookie("authToken");
        res.clearCookie("tenant");
        res.status(401).send("Forbidden");
        return;
      }

      if (req.headers.authorization) {
        res.cookie("authToken", req.headers.authorization);
      }

      const { cookies } = parseReq(req);

      if (req.headers.tenant || cookies.tenant) {
        // first try looking up the specified workspace
        try {
          req.member = await db
            .Member({ tenant: req.headers.tenant || cookies.tenant })
            .findOne({
              email: req.user.get("email"),
            })
            .populate({ match: { deleted: false }, path: "workspace" });
          req.workspace = req.member && req.member.get("workspace");
        } catch (e) {
          // ignore this
        }
      }

      if (!req.workspace) {
        // then try getting the user's first workspace
        req.member = await db
          .Member({ skipTenant: true })
          .findOne({
            email: req.user.get("email"),
          })
          .sort("+createdAt")
          .populate({ match: { deleted: false }, path: "workspace" });
        req.workspace = req.member && req.member.get("workspace");
      }

      if (!req.workspace) {
        // if we still don't have a workspace, then create one
        req.workspace = await db.Workspace().create({
          billing: { expired: false, planId: null },
          name: "My Workspace",
          plan: null,
        });
        req.member = await db
          .Member({ tenant: req.workspace._id })
          .create({ email: req.user.get("email"), role: "ADMIN" });
      }

      res.cookie("tenant", `${req.workspace._id}`);

      bindCurrentNamespace(req, res, () => {
        setCurrentTenantId(req.workspace._id);
        next();
      });
    });
  }

  return app;
};
