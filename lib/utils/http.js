import fetch from "isomorphic-unfetch";

export const getBaseUrl = () =>
  process.browser ? "" : `http://localhost:${process.env.PORT}`;

export default async (path = "", opts = {}, ...args) => {
  const finalOpts = { credentials: "same-origin", ...opts };

  if (typeof window !== "undefined") {
    finalOpts.headers = {
      Authorization: `Bearer ${window.localStorage.getItem("idToken")}`,
      tenant: window.localStorage.getItem("tenant"),
      ...finalOpts.headers,
    };
  }

  if (finalOpts.data) {
    finalOpts.body = JSON.stringify({ ...finalOpts.data });
    delete finalOpts.data;

    finalOpts.headers["Content-Type"] = "application/json";
  }

  const fullPath = path.startsWith("http") ? path : `${getBaseUrl()}${path}`;

  const resp = await fetch(fullPath, finalOpts, ...args);

  if (resp.status < 300) {
    return resp.json();
  }

  return null;
};
