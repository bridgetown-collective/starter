import auth0 from "auth0-js";
import { createBrowserHistory } from "history";

import http from "./http";

const getConfig = require("../config");

export default class Auth {
  auth0;

  history;

  constructor() {
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.handleAuthentication = this.handleAuthentication.bind(this);
    this.isAuthenticated = this.isAuthenticated.bind(this);
    this.getAccessToken = this.getAccessToken.bind(this);
    this.getIdToken = this.getIdToken.bind(this);

    this.auth0 = new auth0.WebAuth({
      domain: getConfig().AUTH0_DOMAIN.split("/")[2],
      clientID: getConfig().AUTH0_CLIENT_ID,
      redirectUri: `${getConfig().APP_URL}/callback`,
      responseType: "token id_token",
      scope: "email profile openid",
    });

    this.history = createBrowserHistory();
  }

  handleAuthentication() {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
      } else if (err) {
        this.history.replace(getConfig().DEFAULT_ROUTE);
      }
    });
  }

  getAccessToken() {
    return window.localStorage.getItem("accessToken");
  }

  getIdToken() {
    return window.localStorage.getItem("idToken");
  }

  getTenant() {
    return window.localStorage.getItem("tenant");
  }

  setSession(authResult) {
    // Set isLoggedIn flag in localStorage
    window.localStorage.setItem("isLoggedIn", "true");

    // Set the time that the Access Token will expire at
    const expiresAt = authResult.expiresIn * 1000 + new Date().getTime();
    window.localStorage.setItem("accessToken", authResult.accessToken);
    window.localStorage.setItem("idToken", authResult.idToken);
    window.localStorage.setItem("expiresAt", expiresAt);

    // navigate after login
    const redirectTo = window.sessionStorage.getItem("redirectTo");
    window.sessionStorage.removeItem("redirectTo");
    window.location.href =
      redirectTo && redirectTo.length ? redirectTo : getConfig().DEFAULT_ROUTE;
  }

  setTenant(tenant) {
    window.localStorage.setItem("tenant", tenant);
  }

  login() {
    // Remove tokens and expiry time
    window.localStorage.setItem("accessToken", null);
    window.localStorage.setItem("idToken", null);
    window.localStorage.setItem("expiresAt", 0);
    window.localStorage.setItem("tenant", null);

    // Remove isLoggedIn flag from localStorage
    window.localStorage.removeItem("isLoggedIn");

    this.auth0.authorize();
  }

  logout() {
    http("/api/logout")
      .catch(e => e)
      .then(() => {
        // Remove tokens and expiry time
        window.localStorage.setItem("accessToken", null);
        window.localStorage.setItem("idToken", null);
        window.localStorage.setItem("expiresAt", 0);
        window.localStorage.setItem("tenant", null);

        // Remove isLoggedIn flag from localStorage
        window.localStorage.removeItem("isLoggedIn");

        this.auth0.logout({
          returnTo: window.location.origin,
        });

        // navigate to the home route
        // window.location = "/";
      });
  }

  isAuthenticated() {
    // Check whether the current time is past the
    // access token's expiry time
    const expiresAt = window.localStorage.getItem("expiresAt");
    return new Date().getTime() < expiresAt;
  }
}
