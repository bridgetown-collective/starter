const aws = require("aws-sdk");
const multer = require("multer");
const multerS3 = require("multer-s3");

const uploadEndpoint = process.env.UPLOAD_ENDPOINT || "";

aws.config = new aws.Config({
  accessKeyId: process.env.UPLOAD_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.UPLOAD_AWS_SECRET_ACCESS_KEY,
});

const s3 = new aws.S3({
  endpoint: new aws.Endpoint(
    uploadEndpoint
      .split(".")
      .slice(1)
      .join("."),
  ),
});

const getUploader = opts => {
  const storage = multerS3({
    s3,
    bucket: uploadEndpoint.split(".")[0],
    acl: "public-read",
    key: (req, file, cb) => {
      cb(
        null,
        `uploads/${opts && opts.directory ? `${opts.directory}/` : ""}${
          req.user._id
        }_${Date.now()}_${file.originalname}`,
      );
    },
    ...opts,
  });

  return multer({ storage });
};

module.exports = getUploader;
