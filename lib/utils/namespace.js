const { createNamespace } = require("cls-hooked");

const namespaceName = "request";
const ns = createNamespace(namespaceName);

const bindCurrentNamespace = (req, res, next) => {
  ns.bindEmitter(req);
  ns.bindEmitter(res);

  ns.run(() => next());
};

const getCurrentTenantId = () => ns.get("tenantId");

const setCurrentTenantId = tenantId => ns.set("tenantId", tenantId);

module.exports = {
  bindCurrentNamespace,
  getCurrentTenantId,
  setCurrentTenantId,
};
