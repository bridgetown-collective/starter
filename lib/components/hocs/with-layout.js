import React from "react";
import PropTypes from "prop-types";
import Head from "next/head";
import Router from "next/router";

import { Box, Button, Heading, Image, Menu, ResponsiveContext } from "grommet";
import { Briefcase, Configure, ContactInfo, Logout } from "grommet-icons";

import getConfig from "../../config";

export default (Page, { authRequired = true, title } = {}) =>
  class WithLayoutComponent extends React.Component {
    static propTypes = {
      auth: PropTypes.object,
      isTransitioning: PropTypes.bool,
      pageProps: PropTypes.any,
    };

    static async getInitialProps(ctx) {
      let pageProps = {};

      if (Page.getInitialProps) {
        pageProps = await Page.getInitialProps(ctx);
      }

      return { pageProps };
    }

    render() {
      const head = (
        <Head>
          <title>
            {getConfig().APP_NAME}
            {title ? ` | ${title}` : ""}
          </title>
        </Head>
      );

      if (
        authRequired &&
        (!this.props.auth ||
          !this.props.auth.service ||
          !this.props.auth.service.isAuthenticated() ||
          !this.props.auth.user)
      ) {
        return head;
      }

      const renderedPage =
        Page instanceof Function ? (
          <Page auth={this.props.auth} {...this.props.pageProps} />
        ) : (
          Page
        );

      return (
        <ResponsiveContext.Consumer>
          {size => (
            <Box align="center" fill>
              {head}

              <Box
                fill="vertical"
                style={{
                  maxWidth: 1366,
                  width: "100%",
                }}
              >
                <Box
                  align="center"
                  direction="row"
                  justify="between"
                  pad={{ left: "medium", right: "small", vertical: "small" }}
                  style={{ zIndex: 1 }}
                  tag="header"
                >
                  <Box
                    direction="row"
                    onClick={() => Router.push(getConfig().DEFAULT_ROUTE)}
                    style={{ cursor: "pointer" }}
                  >
                    <Image
                      src="/static/images/logo.svg"
                      style={{ height: "2em", marginRight: "1em" }}
                    />
                    <Heading
                      color="brand"
                      level="3"
                      margin="none"
                      style={{ fontWeight: "normal" }}
                    >
                      {getConfig().APP_NAME}
                    </Heading>
                  </Box>

                  {this.props.auth.service &&
                  this.props.auth.service.isAuthenticated() &&
                  this.props.auth.workspace ? (
                    <Menu
                      label={
                        <Box align="center" direction="row">
                          {this.props.auth.workspace.name}
                        </Box>
                      }
                      items={[
                        {
                          icon: (
                            <ContactInfo
                              size="small"
                              style={{
                                alignSelf: "center",
                                marginRight: "0.5em",
                              }}
                            />
                          ),
                          label: "Profile",
                          onClick: () => Router.push("/profile"),
                        },
                        {
                          icon: (
                            <Configure
                              size="small"
                              style={{
                                alignSelf: "center",
                                marginRight: "0.5em",
                              }}
                            />
                          ),
                          label: "Workspace Settings",
                          onClick: () => Router.push("/workspace/settings"),
                        },
                        {
                          icon: (
                            <Briefcase
                              size="small"
                              style={{
                                alignSelf: "center",
                                marginRight: "0.5em",
                              }}
                            />
                          ),
                          label: "Switch Workspace",
                          onClick: () => Router.push("/workspaces"),
                        },
                        {
                          label: "Logout",
                          icon: (
                            <Logout
                              size="small"
                              style={{
                                alignSelf: "center",
                                marginRight: "0.5em",
                              }}
                            />
                          ),
                          onClick: () => this.props.auth.service.logout(),
                        },
                      ]}
                    />
                  ) : (
                    <Button
                      color="brand"
                      label="Login"
                      onClick={() => this.props.auth.service.login()}
                      plain
                    />
                  )}

                  <style global jsx>
                    {`
                      header button,
                      [class*="Menu__"] button {
                        border: none !important;
                        box-shadow: none !important;
                      }
                    `}
                  </style>
                </Box>

                <Box direction="row" flex overflow={{ horizontal: "hidden" }}>
                  <div
                    className="sk-folding-cube animated fadeIn"
                    style={{
                      display: this.props.isTransitioning ? "block" : "none",
                      left: "calc(50% - 28px)",
                      position: "absolute",
                      top: "30%",
                    }}
                  >
                    <div className="sk-cube1 sk-cube"></div>
                    <div className="sk-cube2 sk-cube"></div>
                    <div className="sk-cube4 sk-cube"></div>
                    <div className="sk-cube3 sk-cube"></div>
                  </div>

                  <Box
                    className={`animated ${
                      this.props.isTransitioning ? "fadeOut" : "fadeIn"
                    }`}
                    fill="horizontal"
                    pad={size !== "small" ? "xlarge" : "small"}
                    style={{
                      animationDuration: "300ms",
                      margin: "auto",
                      paddingTop: "0",
                    }}
                  >
                    {renderedPage}
                  </Box>
                </Box>
              </Box>
            </Box>
          )}
        </ResponsiveContext.Consumer>
      );
    }
  };
