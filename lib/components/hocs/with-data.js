import React from "react";
import PropTypes from "prop-types";

import http from "../../utils/http";
import parseReq from "../../utils/parse-req";

export default (
  Page,
  { dataKey = "data", path, required = true, ...opts } = {},
) =>
  class WithDataComponent extends React.Component {
    static propTypes = {
      data: PropTypes.any,
    };

    state = {};

    static async getInitialProps(ctx) {
      let pageProps = {};

      if (Page.getInitialProps) {
        pageProps = await Page.getInitialProps(ctx);
      }

      const props = { ...pageProps };

      if (path) {
        const finalOpts = { ...opts };

        const { cookies, origin } = parseReq(ctx.req);

        if (cookies.authToken) {
          finalOpts.headers = {
            ...finalOpts.headers,
            Authorization: cookies.authToken,
            tenant: cookies.tenant,
          };
        }

        if (cookies.authToken || path.startsWith("http")) {
          let fullPath = path.startsWith("http") ? path : `${origin}${path}`;

          if (ctx.query && Object.keys(ctx.query).length) {
            fullPath += `?${Object.entries(ctx.query)
              .map(([k, v]) => `${k}=${v}`)
              .join("&")}`;
          }

          const data = await http(fullPath, finalOpts);
          props[dataKey] = data;
        }
      }

      return props;
    }

    componentDidMount() {
      if (!Object.prototype.hasOwnProperty.call(this.props, dataKey)) {
        http(`${path}${window.location.search}`, opts).then(data =>
          this.setState({ [dataKey]: data }),
        );
      }
    }

    render() {
      if (required && !this.props[dataKey] && !this.state[dataKey]) {
        return null;
      }

      const renderedPage =
        Page instanceof Function ? (
          <Page {...this.state} {...this.props} />
        ) : (
          Page
        );

      return renderedPage;
    }
  };
