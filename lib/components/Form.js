import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "next/router";

import {
  Box,
  Button,
  CheckBox,
  Heading,
  Text,
  TextArea,
  TextInput,
} from "grommet";

class Form extends React.Component {
  static propTypes = {
    fields: PropTypes.array,
    footer: PropTypes.any,
    formProps: PropTypes.object,
    header: PropTypes.any,
    router: PropTypes.object,
    submitLabel: PropTypes.any,
    title: PropTypes.string,
    values: PropTypes.any,
  };

  state = {
    customSetValues: {},
    isLoading: false,
  };

  componentDidMount() {
    const customSetValues = {};

    (this.props.fields || []).forEach(field => {
      const defaultValue = (this.props.values || {})[field.name];
      if (defaultValue) {
        customSetValues[field.name] = defaultValue;
      }
    });

    this.setState({ customSetValues });
  }

  renderField({ fieldType, ...field }) {
    const defaultValue = (this.props.values || {})[field.name];

    const setValue = val => {
      this.setState({
        customSetValues: { ...this.state.customSetValues, [field.name]: val },
      });
    };

    switch (fieldType) {
      case "checkbox":
        return (
          <CheckBox
            checked={!!this.state.customSetValues[field.name]}
            onChange={evt => setValue(evt.target.checked)}
            {...field}
          />
        );
      case "file":
        return <input type="file" {...field} />;
      case "textarea":
        return <TextArea defaultValue={defaultValue} {...field} />;
      default:
        return <TextInput defaultValue={defaultValue} {...field} />;
    }
  }

  render() {
    const { query = {} } = this.props.router || {};
    const { error: errorMessage, message: successMessage } = query;

    return (
      <form
        method="POST"
        onSubmit={() => this.setState({ isLoading: true })}
        {...this.props.formProps}
      >
        <Box gap="medium" justify="between">
          {this.props.title && (
            <Heading level="2" margin="none">
              {this.props.title}
            </Heading>
          )}

          {this.props.header}

          {(this.props.fields || [])
            .filter(f => f && f.name)
            .map((field, idx) => (
              <div key={field.name || idx}>
                {!["checkbox"].includes(field.fieldType) ? (
                  <label>{field.label}</label>
                ) : null}
                {this.renderField({ style: { width: "100%" }, ...field })}
              </div>
            ))}

          {errorMessage ? (
            <Text color="status-error">{errorMessage}</Text>
          ) : null}
          {!errorMessage && successMessage ? (
            <Text color="status-ok">{successMessage}</Text>
          ) : null}

          <Button
            disabled={this.state.isLoading}
            label={this.props.submitLabel || "Submit"}
            margin={{ vertical: "medium" }}
            type="submit"
          />

          {this.props.footer}
        </Box>
      </form>
    );
  }
}

export default withRouter(Form);
