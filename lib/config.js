module.exports = () => {
  const config = {
    APP_LOGO: "/static/images/logo.svg",
    APP_NAME: "Starter",
    APP_URL: global.window
      ? window.location.origin
      : `http://localhost:${process.env.PORT}`,
    AUTH0_CLIENT_ID: "iSB0gwbJ5ng1kpYJuf7f0hb7In1GxbJb",
    AUTH0_DOMAIN: "https://bridgetowncollective.auth0.com/",
    DEFAULT_ROUTE: "/",
    SEGMENT_WRITE_KEY: "yflrDyKnhe7ra8L43cqYZcIK5tyjnFWg",
    STRIPE_PUBLISHABLE_KEY: "pk_test_Ug7m8WngeaqodGUSTk6uGO1y",
  };

  return config;
};
