const fs = require("fs");
const path = require("path");

const app = require("express")();
const nextApp = require("next")({ dev: true });

const nextHandler = nextApp.getRequestHandler();

const walkSync = (dir, filelist = []) => {
  const files = fs.readdirSync(dir);
  let newList = (filelist || []).slice();

  files.forEach(file => {
    if (fs.statSync(dir + file).isDirectory()) {
      newList = walkSync(`${dir + file}/`, newList);
    } else {
      newList.push(dir.replace(__dirname, "") + file);
    }
  });

  return newList;
};

const files = walkSync(path.join(__dirname, "api/"));
files.forEach(filePath => {
  // eslint-disable-next-line
  app.all(filePath.slice(0, -3), require(`.${filePath}`));
});

nextApp.prepare().then(() => {
  app.get("*", (req, res) => nextHandler(req, res));

  app.listen(process.env.PORT, err => {
    if (err) throw err;
    console.log(`> Dev server ready at http://localhost:${process.env.PORT}`);
  });
});
