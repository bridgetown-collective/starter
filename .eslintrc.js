module.exports = {
  extends: [
    "airbnb-base",
    "plugin:react/recommended",
    "plugin:prettier/recommended",
  ],
  env: {
    browser: true,
    jest: true,
    "jest/globals": true,
    node: true,
  },
  parser: "babel-eslint",
  parserOptions: {
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: ["react", "import", "jest"],
  rules: {
    "class-methods-use-this": "off",
    "import/no-named-as-default": "off",
    "import/no-named-as-default-member": "off",
    "no-param-reassign": [
      "error",
      {
        props: true,
        ignorePropertyModificationsFor: ["req"],
      },
    ],
    "no-underscore-dangle": "off",
    quotes: ["error", "double"],
    "react/display-name": "off",
    "react/jsx-filename-extension": [
      "error",
      {
        extensions: [".js"],
      },
    ],
  },
};
