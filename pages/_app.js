import React from "react";
import App from "next/app";
import Router from "next/router";
import { Grommet } from "grommet";

import AuthService from "../lib/utils/auth-service";
import getConfig from "../lib/config";
import http from "../lib/utils/http";
import parseReq from "../lib/utils/parse-req";
import theme from "../lib/theme";

export default class CustomApp extends App {
  state = {
    auth: {},
    authService: null,
    isTransitioning: false,
  };

  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    let authPayload = null;

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    try {
      const { cookies, origin } = parseReq(ctx.req);

      if (cookies.authToken) {
        authPayload = await http(`${origin}/api/current`, {
          headers: { Authorization: cookies.authToken, tenant: cookies.tenant },
        });

        if (!authPayload || !authPayload.user || !authPayload.user.email) {
          authPayload = null;
          ctx.res.clearCookie("authToken");
          ctx.res.clearCookie("tenant");
        }
      }
    } catch (e) {
      // ignore this
    }

    return { authPayload, pageProps };
  }

  initScript(key, authPayload) {
    if (key === "segment") {
      const analytics = window.analytics || [];
      window.analytics = analytics;

      const setAuth = () => {
        const user = authPayload && authPayload.user;
        if (user && analytics.identify instanceof Function) {
          const traits = {};

          traits.avatar = user.photo;
          traits.createdAt = user.createdAt;
          traits.email = user.email;
          traits.id = user._id;
          traits.name = user.name;

          if (authPayload.workspace) {
            traits.company = {
              id: authPayload.workspace._id,
              name: authPayload.workspace.name,
              plan: authPayload.workspace.plan,
            };
          }

          analytics.identify(user._id, traits);
        }
      };

      if (!analytics.initialize) {
        if (analytics.invoked) {
          // console.error("Segment snippet included twice.");
        } else {
          analytics.invoked = true;
          analytics.methods = [
            "trackSubmit",
            "trackClick",
            "trackLink",
            "trackForm",
            "pageview",
            "identify",
            "reset",
            "group",
            "track",
            "ready",
            "alias",
            "debug",
            "page",
            "once",
            "off",
            "on",
          ];

          analytics.factory = t => {
            return (...args) => {
              const e = Array.prototype.slice.call(args);
              e.unshift(t);
              analytics.push(e);
              return analytics;
            };
          };
          for (let t = 0; t < analytics.methods.length; t += 1) {
            const e = analytics.methods[t];
            analytics[e] = analytics.factory(e);
          }
          analytics.load = (t, e) => {
            const n = document.createElement("script");
            n.type = "text/javascript";
            n.async = !0;
            n.src = `https://cdn.segment.com/analytics.js/v1/${t}/analytics.min.js`;
            const a = document.getElementsByTagName("script")[0];
            a.parentNode.insertBefore(n, a);
            analytics._loadOptions = e;
          };

          analytics.SNIPPET_VERSION = "4.1.0";
          analytics.load(getConfig().SEGMENT_WRITE_KEY);
          analytics.page();
          setAuth();
        }
      } else {
        setAuth();
      }
    }
  }

  componentDidMount() {
    Router.events.on("routeChangeStart", () => {
      this.setState({ isTransitioning: true });
    });
    Router.events.on("routeChangeComplete", () => {
      this.setState({ isTransitioning: false });
      if (window.analytics && window.analytics.page instanceof Function) {
        window.analytics.page();
      }
    });

    const auth = new AuthService();
    this.setState({ authService: auth });

    if (/access_token|id_token|error/.test(window.location.hash)) {
      auth.handleAuthentication();
      return;
    }

    if (!auth.isAuthenticated()) {
      this.initScript("segment");
      // auth.login(); // NOTE: enable this to force login
      return;
    }

    const afterAuth = authPayload => {
      this.initScript("segment", authPayload);
      auth.setTenant(
        authPayload && authPayload.workspace && authPayload.workspace._id,
      );
      this.setState({ auth: authPayload || {} });

      if (auth.isAuthenticated()) {
        if (
          getConfig().DEFAULT_ROUTE !== "/" &&
          window.location.pathname === "/"
        ) {
          Router.push(getConfig().DEFAULT_ROUTE);
        }
      }
    };

    const token = auth.getIdToken();
    if (!this.props.authPayload && token && token.length > 20) {
      this.initScript("segment");

      http("/api/current", {
        headers: { Authorization: `Bearer ${token}` },
      }).then(authPayload => {
        if (!authPayload || !authPayload.user || !authPayload.user.email) {
          auth.logout();
        } else {
          afterAuth(authPayload);
        }
      });
    } else {
      afterAuth(this.props.authPayload);
    }
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <Grommet full theme={{ global: theme }}>
        <Component
          {...pageProps}
          auth={{ ...this.state.auth, service: this.state.authService }}
          isTransitioning={this.state.isTransitioning}
        />
      </Grommet>
    );
  }
}
