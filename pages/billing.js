import React from "react";
import PropTypes from "prop-types";

import Subscriptions from "../lib/components/Subscriptions";
import withData from "../lib/components/hocs/with-data";
import withLayout from "../lib/components/hocs/with-layout";

const BillingPage = ({ auth, products }) => {
  return (
    <Subscriptions
      billing={auth && auth.workspace && auth.workspace.billing}
      products={products || []}
    />
  );
};

BillingPage.propTypes = {
  auth: PropTypes.object,
  products: PropTypes.any,
};

export default withLayout(
  withData(BillingPage, {
    dataKey: "products",
    path: "/api/billing/products",
  }),
  {
    title: "Billing",
  },
);
