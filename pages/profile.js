import React from "react";
import PropTypes from "prop-types";

import { Box, Image } from "grommet";

import Form from "../lib/components/Form";
import withLayout from "../lib/components/hocs/with-layout";

const ProfilePage = ({ auth: { user = {} } }) => (
  <Box align="center">
    <Form
      fields={[
        { fieldType: "file", name: "avatar" },
        { label: "Name", name: "name", required: true },
      ]}
      formProps={{
        action: "/api/profile?redirectTo=/profile",
        encType: "multipart/form-data",
      }}
      header={
        user.avatar ? (
          <Box height="xsmall" width="xsmall">
            <Image fit="cover" src={user.avatar} />
          </Box>
        ) : null
      }
      title="Update Profile"
      values={user}
    />
  </Box>
);

ProfilePage.propTypes = {
  auth: PropTypes.any,
};

export default withLayout(ProfilePage, { title: "Profile" });
