import React from "react";
import PropTypes from "prop-types";
import Link from "next/link";

import {
  Accordion,
  AccordionPanel,
  Box,
  Button,
  Heading,
  Paragraph,
} from "grommet";

import Form from "../../lib/components/Form";
import withLayout from "../../lib/components/hocs/with-layout";

const SettingsPage = ({ auth = {} }) => (
  <Box align="center">
    <Heading level="2">Workspace Settings</Heading>

    <Box
      className="divider"
      direction="row"
      flex
      gap="xlarge"
      justify="center"
      margin={{ bottom: "medium" }}
      pad={{ bottom: "medium" }}
    >
      <Form
        fields={[{ label: "Name", name: "name", required: true }]}
        formProps={{
          action: "/api/workspaces/update?redirectTo=/workspace/settings",
        }}
        header={<Heading level="4">Modify Workspace</Heading>}
        submitLabel="Save"
        values={auth.workspace}
      />

      <Box>
        <Heading level="4">Billing</Heading>
        {auth &&
        auth.workspace &&
        auth.workspace.billing &&
        !auth.workspace.billing.expired &&
        auth.workspace.billing.planId ? (
          <Paragraph>
            Currently subscribed to: &quot;{auth.workspace.billing.planLabel}
            &quot;
          </Paragraph>
        ) : (
          <Paragraph>You are currently on the &quot;free&quot; plan</Paragraph>
        )}
        <Link href="/billing">
          <Button label="Modify Subscription" />
        </Link>
      </Box>
    </Box>

    <Accordion>
      <AccordionPanel
        label={
          <Heading color="primary" level="5">
            Advanced
          </Heading>
        }
      >
        <Box pad="medium">
          <Button
            label="Delete Workspace"
            onClick={() => {
              if (
                // eslint-disable-next-line no-alert
                window.confirm(
                  "Are you absolutely sure you want to delete this workspace? This cannot be undone",
                )
              ) {
                window.location =
                  "/api/billing/cancel?redirectTo=/workspaces&deleteWorkspace=true";
              }
            }}
          />
        </Box>
      </AccordionPanel>
    </Accordion>
  </Box>
);

SettingsPage.propTypes = {
  auth: PropTypes.any,
};

export default withLayout(SettingsPage, { title: "Workspace Settings" });
