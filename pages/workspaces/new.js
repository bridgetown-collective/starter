import React from "react";

import { Box } from "grommet";

import getConfig from "../../lib/config";
import Form from "../../lib/components/Form";
import withLayout from "../../lib/components/hocs/with-layout";

const NewWorkspacePage = () => (
  <Box align="center">
    <Form
      fields={[{ label: "Name", name: "name", required: true }]}
      formProps={{
        action: `/api/workspaces/create?redirectTo=${
          getConfig().DEFAULT_ROUTE
        }`,
      }}
      title="Create Workspace"
    />
  </Box>
);

export default withLayout(NewWorkspacePage, { title: "New Workspace" });
