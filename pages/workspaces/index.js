import React from "react";
import PropTypes from "prop-types";
import Link from "next/link";

import { Box, Button, Paragraph } from "grommet";
import { FormAdd, LinkNext } from "grommet-icons";

import getConfig from "../../lib/config";
import withData from "../../lib/components/hocs/with-data";
import withLayout from "../../lib/components/hocs/with-layout";

const WorkspacesPage = ({ workspaces = [] }) => (
  <Box align="center" direction="column" fill="horizontal" flex gap="medium">
    {(workspaces || [])
      .sort(
        (a, b) =>
          (a.name || "").toLowerCase() > (b.name || "").toLowerCase() ? 1 : -1,
        [],
      )
      .map(wk => (
        <Box
          border={{ color: "brand", size: "small" }}
          className="hoverable h-left"
          direction="row"
          fill="horizontal"
          flex
          justify="between"
          key={wk._id}
          onClick={() => {
            window.location = `/api/workspaces/set?tenant=${
              wk._id
            }&redirectTo=${getConfig().DEFAULT_ROUTE}`;
          }}
          pad="small"
          round="small"
          style={{ maxWidth: 800 }}
        >
          <Paragraph margin="none">{wk.name}</Paragraph>
          <LinkNext />
        </Box>
      ))}

    <Link href="/workspaces/new">
      <Button icon={<FormAdd />} label="Add Workspace" plain />
    </Link>
  </Box>
);

WorkspacesPage.propTypes = {
  workspaces: PropTypes.any,
};

export default withLayout(
  withData(WorkspacesPage, {
    dataKey: "workspaces",
    path: "/api/workspaces/list",
  }),
);
