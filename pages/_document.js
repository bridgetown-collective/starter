import React from "react";
import Document, { Head, Main, NextScript } from "next/document";

import getConfig from "../lib/config";
import theme from "../lib/theme";

export default class CustomDocument extends Document {
  render() {
    return (
      <html lang="en">
        <Head>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />

          {/* metatags.io */}
          <meta name="title" content={getConfig().APP_NAME} />
          <meta name="description" content="Bridgetown Collective Starter" />

          <link rel="icon" href={getConfig().APP_LOGO} />

          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"
          />
          <link rel="stylesheet" href="/static/vendor/styles/animate.css" />
          <link rel="stylesheet" href="/static/vendor/styles/spinkit.css" />
          <link rel="stylesheet" href="/static/styles/main.css" />

          <style type="text/css">
            {`
              body {
                margin: 0;
              }

              .hoverable {
                box-shadow: none;
                cursor: pointer;
                transition: box-shadow 300ms ease;
              }
              .hoverable:hover {
                box-shadow: 0 5px ${theme.colors.brand};
              }
              .hoverable.h-left:hover {
                box-shadow: -5px 5px ${theme.colors.brand};
              }
              .hoverable.h-right:hover {
                box-shadow: 5px 5px ${theme.colors.brand};
              }

              .divider {
                border-bottom: solid 1px #ccc;
                width: 100%;
              }
            `}
          </style>

          <script src="https://js.stripe.com/v3/"></script>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
