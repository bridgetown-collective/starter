import React from "react";
import PropTypes from "prop-types";

import withLayout from "../lib/components/hocs/with-layout";

const IndexPage = ({ auth }) => (
  <div>
    <code>{JSON.stringify(auth.user, null, 2)}</code>
    <br />
  </div>
);

IndexPage.propTypes = {
  auth: PropTypes.any,
};

export default withLayout(IndexPage, { authRequired: false });
