import React from "react";
import PropTypes from "prop-types";

import Subscriptions from "../lib/components/Subscriptions";
import withData from "../lib/components/hocs/with-data";
import withLayout from "../lib/components/hocs/with-layout";

const PricingPage = ({ auth, products }) => {
  return (
    <Subscriptions
      billing={auth && auth.workspace && auth.workspace.billing}
      products={products || []}
    />
  );
};

PricingPage.propTypes = {
  auth: PropTypes.object,
  products: PropTypes.any,
};

export default withLayout(
  withData(PricingPage, {
    dataKey: "products",
    path: "/api/billing/products",
  }),
  {
    authRequired: false,
    title: "Pricing",
  },
);
